"""
Name: Milestone Two
Goal: Get the lists to equal the same number of linked listnodes
      (e.g. nodeListOne has five nodes, nodeListTwo has four nodes... append one ListNode to nodeListTwo)
"""
class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        # Establish pointers
        nodeOne = l1
        nodeTwo = l2

        # Traverse the nodeList if able to
        while nodeOne.next:
            # If the OTHER nodeList doesn't contain a pointer to another ListNode within the Linked List
            # Create a new ListNode
            # Append new ListNode to OTHER nodeList
            if nodeTwo.next is None:
                zero_value_node = ListNode(0)
                nodeTwo.next = zero_value_node
                continue
            # Increment the pointers
            nodeOne = nodeOne.next
            nodeTwo = nodeTwo.next

        # Traverse the nodeList if able to
        while nodeTwo.next:
            # If the OTHER nodeList doesn't contain a pointer to another ListNode within the Linked List
            # Create a new ListNode
            # Append new ListNode to OTHER nodeList
            if nodeOne.next is None:
                zero_value_node = ListNode(0)
                nodeOne.next = zero_value_node
                continue
            # Increment the pointers
            nodeTwo = nodeTwo.next
            nodeOne = nodeOne.next

        # Re-establish the pointers to the start of the Linked List of Nodes
        nodeOne = l1
        nodeTwo = l2
        print(nodeOne)
        print(nodeTwo)
        return ""
