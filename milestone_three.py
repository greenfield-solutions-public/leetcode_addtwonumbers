"""
Name: Milestone Three
Goal: Perform the correct calculations for the first three provided test cases
"""
class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        # Establish pointers
        nodeOne = l1
        nodeTwo = l2

        # Traverse the nodeList if able to
        while nodeOne.next:
            # If the OTHER nodeList doesn't contain a pointer to another ListNode within the Linked List
            # Create a new ListNode
            # Append new ListNode to OTHER nodeList
            if nodeTwo.next is None:
                zero_value_node = ListNode(0)
                nodeTwo.next = zero_value_node
                continue
            # Increment the pointers
            nodeOne = nodeOne.next
            nodeTwo = nodeTwo.next

        # Traverse the nodeList if able to
        while nodeTwo.next:
            # If the OTHER nodeList doesn't contain a pointer to another ListNode within the Linked List
            # Create a new ListNode
            # Append new ListNode to OTHER nodeList
            if nodeOne.next is None:
                zero_value_node = ListNode(0)
                nodeOne.next = zero_value_node
                continue
            # Increment the pointers
            nodeTwo = nodeTwo.next
            nodeOne = nodeOne.next

        # Re-establish the pointers to the start of the Linked List of Nodes
        nodeOne = l1
        nodeTwo = l2
        # Create results list of nodes
        results = ListNode(0)
        # Establish a pointer to the beginning of the results
        results_start = results
        # Used if combined value > 9
        carry_over = 0
        # Used to reference the combined values of nodeOne.val and nodeTwo.val
        numbers_added = 0
        # Iterate through nodeOne ListNodes
        while nodeOne.next:
            # Combined the two list nodes value... add carry_over value if present
            numbers_added = nodeOne.val + nodeTwo.val + carry_over
            # Insert the appropriate value
            # (e.g. (9 + 9 = 18) % 10 = 8)
            results.val = numbers_added % 10
            # Calculate if carry_over value is needed
            # (e.g. (18 / 10).__floor__() = 1)
            carry_over = (numbers_added / 10).__floor__()
            # Create new node for the results list of nodes
            results.next = ListNode(0)
            # Iterate to the next ListNode for relevant List of nodes
            nodeOne = nodeOne.next
            nodeTwo = nodeTwo.next
            results = results.next

        # Perform the arithmetic above for the last set of nodes in the list
        numbers_added = nodeOne.val + nodeTwo.val
        # Note: carry_over only valid if list of nodes have one than one node
        results.val = (numbers_added % 10) + carry_over
        # Re-check if carry_over has a value, good for test cases that have one Node within the list
        carry_over = (numbers_added / 10).__floor__()
        # If the last node in the results list of node is 10
        # or carry_over is 1
        # Assign the value of zero
        # Then add a new node with the carry_over value
        if carry_over or results.val is 10:
            results.val = 0
            results.next = ListNode(1)

        print(f"output={results_start}")
        return results_start