import unittest
from utils.Timer import Timer
from milestone_two import Solution
from SinglyLinkList import SinglyLinkedList

# Test Cases

# listone1 = [2, 4, 3]
listone1 = SinglyLinkedList()
listone1.insertAtEnd(2)
listone1.insertAtEnd(4)
listone1.insertAtEnd(3)
# listtwo1 = [5, 6, 4]
listtwo1 = SinglyLinkedList()
listtwo1.insertAtEnd(5)
listtwo1.insertAtEnd(6)
listtwo1.insertAtEnd(4)
# answer1  = [7, 0, 8]
answer1 = SinglyLinkedList()
answer1.insertAtEnd(7)
answer1.insertAtEnd(0)
answer1.insertAtEnd(8)

# listone2 = [0]
# listtwo2 = [0]
# answer2  = [0]
#
# listone3 = [9, 9, 9, 9, 9, 9, 9]
# listtwo3 = [9, 9, 9, 9]
# answer3  = [8, 9, 9, 9, 0, 0, 0, 1]

class AddTwoNumbersTestCases(unittest.TestCase):
    def test_case1(self):
        submission = Solution()
        timer = Timer()
        self.assertEqual(submission.addTwoNumbers(listone1, listtwo1), answer1)
        timer.console_print("after test case1")

    # def test_case2(self):
    #     submission = Solution()
    #     timer = Timer()
    #     self.assertEqual(submission.addTwoNumbers(listone2, listtwo2), answer2)
    #     timer.console_print("after test case2")

    # def test_case3(self):
    #     submission = Solution()
    #     timer = Timer()
    #     self.assertEqual(submission.addTwoNumbers(listone3, listtwo3), answer3)
    #     timer.console_print("after test case3")

if __name__ == '__main__':
    unittest.main()
