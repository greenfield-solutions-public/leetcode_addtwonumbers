class ListNode:
    def __init__(self, data):
        self.val = data
        self.next = None

class SinglyLinkedList:
    def __init__(self):
        self.head = None

    def insertAtFront(self, data):
        new_node = ListNode(data)
        # If first node within Linked List
        if self.head is None:
            # Set the head pointer at created Node
            self.head = new_node
            # End function
            return
        else:
            # Set new nodes' pointer to head
            new_node.next = self.head
            # Set head as the new node
            self.head = new_node

    def insertAtEnd(self, data):
        new_node = ListNode(data)
        # If first node within Singly Linked List
        if self.head is None:
            # Assign head as the new_node
            self.head = new_node
            # Exit method
            return
        # Start at first node within Singly Linked List
        current_node = self.head
        # While there are more nodes within Singly Linked List, continue iteration
        while(current_node.next):
            current_node = current_node.next
        # Assign the last node's pointer to the newly created new_node
        current_node.next = new_node

    def print(self):
        current_node = self.head
        while(current_node):
            print(current_node.val)
            current_node = current_node.next