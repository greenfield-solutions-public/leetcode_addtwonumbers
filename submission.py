class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        nodeOne = l1
        nodeTwo = l2
        while nodeOne.next:
            if nodeTwo.next is None:
                zero_value_node = ListNode(0)
                nodeTwo.next = zero_value_node
                continue
            nodeOne = nodeOne.next
            nodeTwo = nodeTwo.next
        while nodeTwo.next:
            if nodeOne.next is None:
                zero_value_node = ListNode(0)
                nodeOne.next = zero_value_node
                continue
            nodeTwo = nodeTwo.next
            nodeOne = nodeOne.next
        nodeOne = l1
        nodeTwo = l2
        results = ListNode(0)
        results_start = results
        carry_over = 0
        numbers_added = 0
        while nodeOne.next:
            numbers_added = nodeOne.val + nodeTwo.val + carry_over
            results.val = numbers_added % 10
            carry_over = (numbers_added / 10).__floor__()
            results.next = ListNode(0)
            nodeOne = nodeOne.next
            nodeTwo = nodeTwo.next
            results = results.next
        numbers_added = nodeOne.val + nodeTwo.val
        results.val = (numbers_added % 10) + carry_over
        carry_over = (numbers_added / 10).__floor__()
        if carry_over or results.val >= 10:
            results.val = results.val % 10
            results.next = ListNode(1)
        return results_start